import * as React from "react";
import { Helmet } from "react-helmet";
import metaSettings from "../../../content/settings/meta.yml";

const Seo = (props) => {
  return (
    <Helmet
      htmlAttributes={{lang: `${metaSettings.language}`}}
      title={props.title}
      link={metaSettings.linkitems}
      meta={metaSettings.metatags
        .filter((tag) => tag.isdynamic === false)
        .map((tag) => tag)
        .concat(
          metaSettings.metatags
            .filter((tag) => tag.isdynamic === true)
            .map((tag) => {
              return {
                property: `${tag.property}`,
                content: `${props[tag.propertylookupname]}`,
              };
            })
        )}
    />
  );
};

export default Seo;
