import * as React from 'react';
import { Col } from 'react-bootstrap';
import footerSettings from '../../../content/settings/footer.yml';
import * as styles from './footer.module.scss';

const Footer = () => {
  return (
    <>
      <footer className={`py-3 ${styles.footer}`}>
        <div className={`container-sm ${styles.footerContainer}`}>
          <Col sm={3}>© {footerSettings.copywrite}</Col>
          <Col xs={3} sm={2} className={`${styles.linkContainer}`}>
            {footerSettings.linkitems.map( (item, idx) => 
              <a key={idx} href={`${item.path}`} className={`${styles.link}`}>{item.name}</a>
            )}
          </Col>
        </div>
      </footer>
    </>
  );
};

export default Footer;