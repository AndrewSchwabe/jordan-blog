import * as React from 'react';
import { Row } from 'react-bootstrap';
import ContentCard from '../content-card';
import * as styles from './recent-posts.module.scss';

const RecentPosts = ({
  data
}) => {
  const edges =[];
  return (
    <>
      {edges && edges > 0 ? (
        <Row className={`py-3`}>
          <h4 className={`${styles.title}`}>Recent Posts</h4>
          {edges.map(({ node }) => (
            <ContentCard
              title={node.frontmatter.title}
              date='2022-07-02'
              excerpt='this is a sample excerpt'
            />
          ))}
        </Row>
      ) : (
        <></>
      )}
    </>
  );
};

export default RecentPosts;
