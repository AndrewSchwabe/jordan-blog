import * as React from 'react';
import PropTypes from 'prop-types';
import Header from '../header';
import Footer from '../footer';
import * as styles from './layout.module.scss';

const Layout = ({ children }) => {
  return (
    <>
      <Header siteTitle='The Jordan Wagner' />
      <div className={`container-sm ${styles.containerBody}`}>
        <main>{children}</main>
      </div>
      <Footer />
    </>
  );
};

Layout.defaultProps = {
  children: [],
};

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;