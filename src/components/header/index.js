import * as React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'gatsby';
import { Nav, Navbar } from 'react-bootstrap';
import headerSettings from '../../../content/settings/header.yml';
import * as styles from './header.module.scss';

const Header = () => (
  <Navbar className={`px-5 ${styles.navbarStyle}`} expand='lg' variant='dark'>
    <Navbar.Brand>
      <Link className={`navbar-brand ${styles.siteTitleStyle}`} to='/'>
        {headerSettings.title}
      </Link>
    </Navbar.Brand>
    <Navbar.Toggle aria-controls='basic-navbar-nav' />
    <Navbar.Collapse id='basic-navbar-nav'>
      <Nav className='mr-auto'>
        {headerSettings.linkitems.map((item, idx) => 
          <Nav.Item as='li' key={idx}>
            <Link to={`${item.path}`} className='nav-link'>
              {item.name}
            </Link>
          </Nav.Item>
        )}
      </Nav>
    </Navbar.Collapse>
  </Navbar>
);

Header.defaultProps = {
  siteTitle: ``,
};

Header.propTypes = {
  siteTitle: PropTypes.string,
};

export default Header;