const path = require(`path`);
const { getHeapSnapshot } = require("v8");

exports.createPages = async ({ graphql, actions }) => {
  const { createRedirect, createPage } = actions;

  addRedirects(createRedirect);

  const result = await graphql(`
    query PageContent {
      pages: allMdx(filter: { frontmatter: { iscontentpage: { eq: true } } }) {
        edges {
          node {
            id
            slug
          }
        }
      }
      posts: allMdx(filter: { frontmatter: { iscontentpage: { eq: false } } }) {
        edges {
          node {
            id
            slug
          }
        }
      }
    }
  `);

  await addPages(
    result.data.pages.edges,
    `src/templates/contentPageTemplate/content-page-template.js`,
    createPage);

  await addPages(
    result.data.posts.edges,
    `src/templates/postPageTemplate/post-page-template.js`,
    createPage);
};

addRedirects = (createRedirect) => {
  createRedirect({
    fromPath: "/sitemap.xml",
    toPath: "/sitemap/sitemap-index.xml",
    isPermanent: true,
  });
};

addPages = async (edges, templatePath, createPage) => {
  const contentTemplate = path.resolve(templatePath);

  edges.map(({ node }) =>
    createPage({
      path: node.slug === "" ? "/" : node.slug,
      component: contentTemplate,
      context: {
        nodeId: node.id,
      },
    })
  );
};