module.exports = {
  siteMetadata: {
    title: `The Jordan Wagner`,
    siteUrl: `https://thejordanwagner.com`,
    description: `Personal Blog for Jordan Wagner`,
    author: `Jordan Wagner`,
  },
  plugins: [
    "gatsby-plugin-netlify-cms",
    "gatsby-plugin-sass",
    "gatsby-plugin-react-helmet",
    "gatsby-plugin-sitemap",
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "pages",
        path: "./content/pages",
      },
      __key: "pages",
    },
    "gatsby-plugin-client-side-redirect",
    {
      resolve: "gatsby-plugin-mdx",
      options: {
        extensions: [`.md`, `.mdx`],
      },
    },
  ],
};
